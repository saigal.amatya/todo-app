import React, { Component } from "react";
import { connect } from "react-redux";

import TodoList from "./components/TodoList";
import "semantic-ui-css/semantic.min.css";
import AddTodo from "./components/AddTodo";
import { Container } from "semantic-ui-react";

class App extends Component {
  render() {
    return (
      <Container>
        <TodoList />
        <AddTodo />
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  todos: state.todoReducer.todos
});

export default connect(mapStateToProps)(App);
