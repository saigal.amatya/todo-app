import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Image, Item } from "semantic-ui-react";

class TodoList extends Component {
  static propTypes = {
    todos: PropTypes.array.isRequired
  };

  render() {
    console.log(this.props);
    return (
      <Item.Group>
        {this.props.todos.map(todo => (
          <Item key={todo.id}>
            <Item.Image size="tiny" src="/images/wireframe/image.png" />

            <Item.Content>
              <Item.Header as="a">{todo.title}</Item.Header>
              <Item.Meta>{todo.task}</Item.Meta>
              <Item.Description>
                <Image src="/images/wireframe/short-paragraph.png" />
              </Item.Description>
              <Item.Extra>Additional Details</Item.Extra>
            </Item.Content>
          </Item>
        ))}
      </Item.Group>
    );
  }
}

const mapStateToProps = state => ({
  todos: state.todoReducer.todos
});

export default connect(mapStateToProps)(TodoList);
