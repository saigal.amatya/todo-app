import React, { Component } from "react";
import { connect } from "react-redux";
import { Button, Form } from "semantic-ui-react";
import { addTodo } from "../actions/todos";
import uuid from "uuid";

class AddTodo extends Component {
  state = {
    id: "",
    title: "",
    task: ""
  };

  handleChange = e =>
    this.setState({
      [e.target.name]: e.target.value
    });

  onSubmit = e => {
    e.preventDefault();
    console.log(e);
    this.setState({
      id: uuid.v4()
    });
    const { id, title, task } = this.state;
    const todo = { id, title, task };
    this.props.addTodo(todo);
  };

  render() {
    const { task, title } = this.state;
    return (
      <Form onSubmit={this.onSubmit}>
        <Form.Field>
          <label>Task Title</label>
          <input
            placeholder="Enter Task Title"
            name="task"
            value={task}
            onChange={this.handleChange}
          />
        </Form.Field>
        <Form.Field>
          <label>Last Name</label>
          <input
            placeholder="Enter Task Description"
            name="title"
            value={title}
            onChange={this.handleChange}
          />
        </Form.Field>
        <Button type="submit">Submit</Button>
      </Form>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addTodo: todo => dispatch(addTodo(todo))
  };
};

export default connect(
  null,
  mapDispatchToProps
)(AddTodo);
