import {
  LIST_TODO,
  ADD_TODO,
  DELETE_TODO,
  UPDATE_TODO
} from "../actions/types";

const initialState = {
  todos: [
    { id: 1, task: "Go to the store", title: "Shopping" },
    { id: 2, task: "Create a new Project", title: "Project" },
    { id: 3, task: "Watch a movie!", title: "Leisure" }
  ]
};

export default function(state = initialState, action) {
  switch (action.type) {
    case ADD_TODO:
      return {
        ...state,
        todos: [...state.todos, action.payload]
      };

    case DELETE_TODO:
      return {
        ...state,
        todos: state.leads.filter(todo => todo.id !== action.payload.id)
      };

    case LIST_TODO:
      return {
        ...state
      };

    default:
      return {
        ...state
      };
  }
}
