import { ADD_TODO } from "./types";

export const addTodo = todo => (dispatch, getState) => {
  dispatch({ type: ADD_TODO, payload: todo });
};
